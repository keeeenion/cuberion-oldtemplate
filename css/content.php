<?
/* Get requested page */
$page = $_GET["p"];
$pages = array("members", "software", "graphics", "forum");
if (!in_array($page, $pages)) {
	header("Location: http://www.cuberion.eu");
	die();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="description" content="Cuberion">
    <meta name="author" content="Cuberion">
	<title>Cuberion - <? echo ucfirst($page); ?></title>
    
    <!-- css -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/content-style.css" rel="stylesheet">
    
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
        
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Cuberion</a>
            </div>
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">Software</a>
                    </li>
                    <li>
                        <a href="#">Graphics</a>
                    </li>
                    <li>
                        <a href="#">Members</a>
                    </li>
                </ul>
            </div>
            
        </div>
    </nav>

    <!-- Content -->
    <div class="container">

        <!-- Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><? echo ucfirst($page); ?></h1>
            </div>
        </div>

        <!-- Members -->
        <?
		print "
        <div class='row'>
        
            <div class='col-xs-12 ccol-sm-12 col-md-4 col-lg-4'>
				<img class='img-responsive center-block' src='http://placehold.it/500x600' alt=''>
            </div>
            
            <div class='col-xs-12 ccol-sm-12 col-md-8 col-lg-8'>
                <h2>Keeeenion <small>[CEO]</small></h2>
                <hr>
                <h4>Description</h4>
                <p>Mingi Description</p>
                <h4>Skills / Professions</h4>
                <p>Jalka</p>
                <h4>Hobbies</h4>
                <p>CSS, HTML</p>
            </div>
            
        </div>
		";
		?>
        
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015 Cuberion. All rights reserved.</p>
                    <p>Keeeenion@Cuberion.eu</p>
                </div>
            </div>
        </footer>

    </div>

    <!-- Javascript -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>