<?
// Get requested page
$page = $_GET["p"];
$pages = array("members", "software", "graphics", "forum");
if (!in_array($page, $pages)) {
	header("Location: http://www.cuberion.eu");
	die();
}
$servername = "MISSING";
$username = "MISSING";
$password = "MISSING";
$dbname = "MISSING";

// Create connection
$mysqli = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$mysqli) {
	die("Connection to the database failed.");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="description" content="Cuberion">
    <meta name="author" content="Cuberion">
	<title>Cuberion - <? echo ucfirst($page); ?></title>
    
    <!-- css -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/content-style.css" rel="stylesheet">
    
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
        
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://www.cuberion.eu">Cuberion</a>
            </div>
        </div>
    </nav>

    <!-- Content -->
    <div class="container">

        <!-- Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><? echo ucfirst($page); ?></h1>
            </div>
        </div>

        <!-- Members -->
        <?
		for ($x = 1; $x <= mCount($mysqli); $x++) {
			$result = mysqli_query($mysqli, "SELECT * FROM Members WHERE ID = '$x'");
			$array = mysqli_fetch_array($result);
			$Country_s = $array['Country_s'];
			$Nickname = $array['Nickname'];
			$Firstname = $array['Firstname'];
			$Surname = $array['Surname'];
			$Job = $array['Job'];
			$Desc = nl2br($array['Description']);
			$Skills = nl2br($array['Skills']);
			$Email = nl2br($array['Email']);
			$Skype = nl2br($array['Skype']);
			$Siganture = $array['Signature'];
		print "
        <div class='row'>
        
            <div class='col-xs-11 ccol-sm-11 col-md-4 col-lg-4'>
				<img class='img-responsive center-block' src='img/members/$Nickname.png'>				
            </div>
			
			<div class='center-block col-xs-1 ccol-sm-1 col-md-1 col-lg-1'>
				<ul class='social-network'>
						<li style='$fb_p'><a href='https://www.facebook.com/$fb'>
						<span class='fa-stack fa-2x'>
							<i class='fa fa-circle fa-stack-2x'></i>
							<i class='fa fa-facebook fa-stack-1x fa-inverse'></i>
						</span></a>
						</li>
						<li style='$tw_p'><a href='https://twitter.com/$tw'>
						<span class='fa-stack fa-2x'>
							<i class='fa fa-circle fa-stack-2x'></i>
							<i class='fa fa-twitter fa-stack-1x fa-inverse'></i>
						</span></a>
						</li>
						<li style='$gh_p'><a href='https://github.com/$gh'>
						<span class='fa-stack fa-2x'>
							<i class='fa fa-circle fa-stack-2x'></i>
							<i class='fa fa-github fa-stack-1x fa-inverse'></i>
						</span></a>
						</li>
						<li style='$bb_p'><a href='https://bitbucket.org/$bb'>
						<span class='fa-stack fa-2x'>
							<i class='fa fa-circle fa-stack-2x'></i>
							<i class='fa fa-bitbucket fa-stack-1x fa-inverse'></i>
						</span></a>
                        <li style='$yt_p'><a href='https://www.youtube.com/channel/$yt'>
						<span class='fa-stack fa-2x'>
							<i class='fa fa-circle fa-stack-2x'></i>
							<i class='fa fa-youtube fa-stack-1x fa-inverse'></i>
						</span></a>
						</li>
				</ul>
			</div>
			
            <div class='col-xs-12 ccol-sm-12 col-md-7 col-lg-7'>
            	<h2><img src='http://flagpedia.net/data/flags/mini/$Country_s.png'/> $Nickname<small> [$Job]</small></h2>
				<p>Email: $Email</p>
				<p>Skype: $Skype</p>
                <hr>
                <h4>Description</h4>
                <p>$Desc</p>
                <h4>Skills / Professions</h4>
                <p>$Skills</p>
				<hr>
				<p>$Siganture</p>
            </div>
            
        </div>
		";
		}
		?>
        
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015 Cuberion. All rights reserved.</p>
                    <p>Keeeenion@Cuberion.eu</p>
                </div>
            </div>
        </footer>

    </div>
    
    <?
	// Finds the amount of members in the database
	function mCount($mysqli) {
			$row_cnt = 0;
			if ($result = mysqli_query($mysqli, "SELECT ID FROM Members")) {
				$memCount = mysqli_num_rows($result);
			}
			return $memCount;
	}

    <!-- Javascript -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>