<?php
class Faction // Pr�sence du mot-cl� class suivi du nom de la classe.
{
	private $_name;       
	private $_description;
	private $_leader; 
	private $_players;
	private $_power; 
	

	public function __construct($name, $description,$l,$p,$po)
	  {
		$this->_name=$name; 
		$this->_description = $description;
		$this->_leader = $l;
		$this->_players = $p;
		$this->_power = $po;
	  }	
	  
	public function getName()
	{
		return $this->_name;
	}
	
	public function getDesc()
	{
		return $this->_description;
	}
	
	public function getLeader()
	{
		return $this->_leader;
	}
	
	public function getPlayers()
	{
		return $this->_players;
	}
	
	public function getPower()
	{
		return $this->_power;
	}
  
	  
	  public static function getAll()
		{
			$fp = @fsockopen("5.101.102.251", 8020);
			if($fp)
			{
				fwrite($fp, "info\r\n");
				$scan = fgets($fp);

				$stack = array();
				for ($i = 0; $i < $scan; $i++) 
				{
					$faction = new Faction(fgets($fp),fgets($fp),fgets($fp),fgets($fp),fgets($fp));
					array_push($stack, $faction);	
		
				}
				
				return $stack;
			}
		}
  
  
}
?>